from . import models 

from odoo.api import Environment, SUPERUSER_ID

def uninstall_hook(cr, registry):
    pass

def post_init_hook(cr, registry):
    env = Environment(cr, SUPERUSER_ID, {})
    tier_model = env['res.partner.tier']

    if tier_model.search_count([]) == 0:
        tier = tier_model.create({
            'name': 'Basic Tier',
            'discount_percentage': 0.0,
        })
