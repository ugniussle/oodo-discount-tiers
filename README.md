# Customer Tier Discount addon for Odoo 15.0

## Setup Instructions
1. Install Odoo 15.0 server [locally](https://www.odoo.com/documentation/15.0/administration/on_premise/source.html).
2. Pull this repository to odoo server addons or separate addon directory.

## Running Instructions
1. Run `python3 odoo-bin -d db_name` (if this addon was pulled to a separate directory add `--addons "./addons/,path/to/addons/directory"`)
2. Install the *Customer Tier Discounts* addon.

## Usage
1. You can add partner discount tiers by navigating *Main Menu -> Invoicing -> Customers -> Partner Tiers*. A starter Basic Tier with a discount of 0% should already be created. Discount ranges from 0.0 (no discount) to 100.0 (everything is free).
2. Now you can set a tier to a partner by Navigating *Main Menu -> Invoicing -> Customers -> Customers*. Selecting a customer in this list and clicking on the *Sales & Purchase* tab allows you to change the tier of a partner.
3. When creating new orders or updating old ones with this partner the discount will be applied automatically. This discount is shown below customer info.

## Notes
- A partner with no tier has no discount applied.
- Tiers must be manually applied, by default customers have no tier.

## Testing Instructions
To test this addon, run Odoo server with the argument `--test-tags '+discount_tiers'`.


