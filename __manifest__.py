 # -*- coding: utf-8 -*-

{
    'name': 'Customer Tier Discounts',
    'version': '0.1',
    'category': 'Sales/Sales',
    'sequence': 5,
    'summary': 'Add discounts to orders based on customer tier',
    'description': """ """,
    'website': 'https://gitlab.com/ugniussle/oodo-discount-tiers',
    'depends': ['base', 'sale_management'],
    'data': [
        'security/ir.model.access.csv',
        'views/discount_tier_views.xml',
        'views/discount_tier_menus.xml',
        'views/res_partner_views.xml',
    ],
    'demo': [

    ],
    'application': True,
    'uninstall_hook': 'uninstall_hook',
    'post_init_hook': 'post_init_hook',
    'license': 'LGPL-3'
}
