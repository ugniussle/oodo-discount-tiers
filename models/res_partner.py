from odoo import models, fields, api

class ResPartner(models.Model):
    _inherit = 'res.partner'

    tier_id = fields.Many2one("res.partner.tier", string="Partner Tier")

    discount_text = fields.Char(compute="_compute_discount_text", string="Discount")

    @api.depends("tier_id")
    def _compute_discount_text(self):
        for record in self:
            tier = record.tier_id
            if tier['id'] == False:
                record.discount_text = "No tier: 0.0%"
                return

            discount_str = str(tier.discount_percentage)
            record.discount_text = tier.name + ": " + discount_str + "%"
