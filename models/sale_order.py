from odoo import models, fields, api

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    discount_tier = fields.Char(related="partner_id.discount_text")

    @api.model
    def create(self, values):
        res = super(SaleOrder, self).create(values)
        self.update_tier_discounts()

        return res

    def write(self, values):
        res = super(SaleOrder, self).write(values)
        for order in self:
                self.update_tier_discounts()

        return res

    def update_tier_discounts(self):
        for order in self:
            if (order.state == 'done' or order.state == 'cancelled'):
                continue

            order_lines = order['order_line']
            partner = order['partner_id']
            tier = partner.mapped('tier_id')

            for line in order_lines:
                line.discount = tier.discount_percentage

    @api.onchange("partner_id")
    def _onchange_partner_id(self):
        self._update_discount()

    @api.onchange("order_line")
    def _onchange_order_line(self):
        self._update_discount()

    def _update_discount(self):
        partner = self.partner_id
        if partner['id'] == False:
            return

        tier = partner.mapped('tier_id')
        for line in self.order_line:
            line.discount = tier.discount_percentage

