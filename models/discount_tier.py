from odoo import models, fields, api
from odoo.exceptions import ValidationError

class ResPartnerTier(models.Model):
    _name = "res.partner.tier"
    _description = "Stores information on the different customer discount tiers."
    name = fields.Char(string="Name", required=True)
    discount_percentage = fields.Float(
        string="Discount Percentage",
        help="Percentage by which the price is reduced",
        required=True,
        default=0.0
    )

    @api.constrains('discount_percentage')
    def check_discount(self):
        for record in self:
            if record.discount_percentage < 0.0:
                raise ValidationError("The discount must be positive")
            if record.discount_percentage > 100.0:
                raise ValidationError("The discount cannot exceed 100%")

