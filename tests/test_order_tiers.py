from odoo.tests import tagged
from odoo.tools import float_compare
from odoo.exceptions import ValidationError
from odoo.addons.sale.tests.common import TestSaleCommon

@tagged('-at_install', 'post_install', 'discount_tiers')
class TestSaleOrderTiers(TestSaleCommon):

    @classmethod
    def setUpClass(cls, chart_template_ref=None):
        super().setUpClass(chart_template_ref=chart_template_ref)

        SaleOrder = cls.env['sale.order'].with_context(tracking_disable=True)
        ResPartnerTier = cls.env['res.partner.tier'].with_context(tracking_disable=True)

        cls.tier0 = ResPartnerTier.create({
            'name': 'tier_0',
            'discount_percentage': 0.0,
        })

        cls.tier20 = ResPartnerTier.create({
            'name': 'tier_20',
            'discount_percentage': 20.0,
        })

        # partner with no tier
        cls.partner_c = cls.partner_a.copy({})
        # partner with 0% discount
        cls.partner_a.tier_id = cls.tier0
        # partner with 20% discount
        cls.partner_b.tier_id = cls.tier20

        cls.sale_order_a = SaleOrder.create({
            'partner_id': cls.partner_a.id,
            'partner_invoice_id': cls.partner_a.id,
            'partner_shipping_id': cls.partner_a.id,
            'pricelist_id': cls.company_data['default_pricelist'].id,
        })

        cls.sale_order_b = SaleOrder.create({
            'partner_id': cls.partner_b.id,
            'partner_invoice_id': cls.partner_b.id,
            'partner_shipping_id': cls.partner_b.id,
            'pricelist_id': cls.company_data['default_pricelist'].id,
        })

        cls.sale_order_c = SaleOrder.create({
            'partner_id': cls.partner_c.id,
            'partner_invoice_id': cls.partner_c.id,
            'partner_shipping_id': cls.partner_c.id,
            'pricelist_id': cls.company_data['default_pricelist'].id,
        })

        cls.order_line_a = cls.env['sale.order.line'].create({
            'name': cls.company_data['product_order_no'].name,
            'product_id': cls.company_data['product_order_no'].id,
            'product_uom_qty': 2,
            'product_uom': cls.company_data['product_order_no'].uom_id.id,
            'price_unit': cls.company_data['product_order_no'].list_price,
            'order_id': cls.sale_order_a.id,
            'tax_id': False,
        })

        cls.order_line_b = cls.order_line_a.copy({'order_id': cls.sale_order_b.id})
        cls.order_line_c = cls.order_line_a.copy({'order_id': cls.sale_order_c.id})

        cls.sale_order_a.update_tier_discounts()
        cls.sale_order_b.update_tier_discounts()
        cls.sale_order_c.update_tier_discounts()

    def test_discount_application(self):
        self.assertEqual(float_compare(self.order_line_a.discount, 0.0, precision_digits=2), 0)
        self.assertEqual(float_compare(self.order_line_b.discount, 20.0, precision_digits=2), 0)
        self.assertEqual(float_compare(self.order_line_c.discount, 0.0, precision_digits=2), 0)

        self.assertEqual(float_compare(self.sale_order_a.amount_total, 560.0, precision_digits=2), 0)
        self.assertEqual(float_compare(self.sale_order_b.amount_total, 448.0, precision_digits=2), 0)
        self.assertEqual(float_compare(self.sale_order_c.amount_total, 560.0, precision_digits=2), 0)

    def test_order_status(self):
        self.sale_order_a.state = 'done'
        self.partner_a.tier_id = self.tier20
        self.sale_order_a.update_tier_discounts()
        # sale order's price was not changed
        self.assertEqual(float_compare(self.sale_order_a.amount_total, 560.0, precision_digits=2), 0)

        self.sale_order_a.state = 'draft'
