from odoo.tests import tagged
from odoo.tools import float_compare
from odoo.exceptions import ValidationError
from odoo.tests.common import SingleTransactionCase

@tagged('-at_install', 'post_install', 'discount_tiers')
class TestSaleOrderTiers(SingleTransactionCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        ResPartnerTier = cls.env['res.partner.tier'].with_context(tracking_disable=True)

        cls.tier0 = ResPartnerTier.create({
            'name': 'tier_0',
            'discount_percentage': 0.0,
        })

        cls.tier20 = ResPartnerTier.create({
            'name': 'tier_20',
            'discount_percentage': 20.0,
        })

    def test_partner_tier(self):
        ResPartner = self.env['res.partner'].with_context(tracking_disable=True)
        
        partner_1 = partner_2 = None
        try:
            partner_1 = ResPartner.create({
                'name': 'partner_1',
                'tier_id': self.tier0.id
            })
        except:
            self.fail("Cannot create partner with tier0")

        try:
            partner_2 = ResPartner.create({
                'name': 'partner_2',
                'tier_id': self.tier20.id
            })
        except:
            self.fail("Cannot create partner with tier20")

        self.assertEqual(float_compare(partner_1.tier_id.discount_percentage, 0.0, precision_digits=2), 0)
        self.assertEqual(float_compare(partner_2.tier_id.discount_percentage, 20.0, precision_digits=2), 0)

        try:
            partner_1.tier_id = self.tier20
        except:
            self.fail("Cannot update partner with tier20")

        try:
            partner_2.tier_id = self.tier0
        except:
            self.fail("Cannot update partner with tier0")

        self.assertEqual(float_compare(partner_1.tier_id.discount_percentage, 20.0, precision_digits=2), 0)
        self.assertEqual(float_compare(partner_2.tier_id.discount_percentage, 0.0, precision_digits=2), 0)
