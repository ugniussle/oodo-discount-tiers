from odoo.tests import tagged
from odoo.tests.common import SingleTransactionCase
from odoo.exceptions import ValidationError

@tagged('-at_install', 'post_install', 'discount_tiers')
class TestResPartnerTier(SingleTransactionCase):

    def test_discount_validation(self):
        with self.assertRaises(ValidationError):
            tier150 = self.env['res.partner.tier'].create({
                'name': 'tier_150',
                'discount_percentage': 150.0,
            })

        with self.assertRaises(ValidationError):
            tier_negative = self.env['res.partner.tier'].create({
                'name': 'tier_150',
                'discount_percentage': -100.0,
            })

        try:
            tier0 = self.env['res.partner.tier'].create({
                'name': '_tier0',
                'discount_percentage': 0.0,
            })
        except ValidationError:
            self.fail("Cannot create 0.0 discount")

        try:
            tier100 = self.env['res.partner.tier'].create({
                'name': 'tier100',
                'discount_percentage': 100.0,
            })
        except ValidationError:
            self.fail("Cannot create 100.0 discount")
